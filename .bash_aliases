alias ll="ls --group-directories-first -alh";
alias l="ls --group-directories-first -alh";

alias chalias="vim .bash_aliases"
#alias powertop="sudo powertop --auto-tune"
alias service="sudo service --status-all"
alias ins='sudo apt-get install'
alias update='sudo apt update && sudo apt upgrade'
alias netstat='sudo netstat -ap'

alias cd..="cd ..";
alias ..="cd ..";
alias ...="cd ../..";
alias ~="cd ~";
alias ifconfig='sudo ifconfig'
alias svim='sudo vim'
alias smc='sudo mc'

alias zik="ncmpcpp"
ddg () { w3m https://duckduckgo.com/lite?q="$*"; }
wikifr () { w3m https://fr.wikipedia.org/wiki/Special:Search?search="$*"; }
wiki () { w3m https://wikipedia.org/wiki/Special:Search?search="$*"; }
alias rts='w3m rts.ch/info'
alias meteo='curl http://www.wttr.in/44.477,9.056'
alias klaxon='mpv /home/kingburger/Musique/radio_klaxon.m3u'

alias low='xrandr --output LVDS-0 --brightness 0.4'
alias high='xrandr --output LVDS-0 --brightness 1'

alias sdn='sudo shutdown now'

#for music over ssh
alias v+="amixer set Master 10%+";
alias v-="amixer set Master 10%-";
alias next="audtool --playlist-advance";
alias play="audtool --playback-pause";
alias auds="audtool --playlist-shuffle-toggle";
alias audi="audtool --current-song";

#work with ftp
alias mc_ftp_c4n4t3_tk="mc cd ftp://ftpupload.net /";
alias mc_ftp_zircologik_ch="mc cd sftp://217.115.150.2";

# recherche de fichiers
function ff() { sudo find . -type f -iname $* -ls ; };
